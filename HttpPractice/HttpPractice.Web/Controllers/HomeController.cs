﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Ajax;
using Newtonsoft.Json;

namespace HttpPractice.Web.Controllers
{
    public class HomeController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Json()
        {
            var user = new {name = "John", email = "john@mail.ru"};

            return Json(user, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public Int32 Sum(Int32 a, Int32 b)
        {
            return a + b;
        }

        [HttpGet]
        public ActionResult ResponseWithCookies()
        {
            HttpContext.Response.Cookies.Add(new HttpCookie("Date", DateTime.Now.ToShortTimeString()));

            return Content("В этом ответе должны быть куки");
        }

        [HttpGet]
        public ActionResult ResponseWithCustomHeader()
        {
            Math.Round(0,0,MidpointRounding.ToEven)
            HttpContext.Response.Headers.Add("OurCustomDateHeader",DateTime.Now.ToShortTimeString());

            return Content("В этом ответе должен быть кастомный заголовок");
        }


    }
}